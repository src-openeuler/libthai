Name: libthai
Version: 0.1.29
Release: 2
Summary: Libaray for Thai language
License: LGPLv2+
URL: http://linux.thai.net
Source: https://linux.thai.net/pub/thailinux/software/libthai/libthai-%{version}.tar.xz

BuildRequires: gcc
BuildRequires: pkgconfig(datrie-0.2)
BuildRequires: doxygen


%description
LibThai is a C library for developers who need Thai language support in their
programs. It includes Thai character set support, Thai character properties,
Thai string manipulators, Thai string collation, Thai word breaking, Thai
input method and Thai output method.

%package devel
Summary:  Thai language support routines
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig


%description devel
The development package for Libthai.

%package static
Summary: libthai static library
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description static
The static library for libthai.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT

%makeinstall

mkdir installed-docs
mv $RPM_BUILD_ROOT%{_docdir}/libthai/* installed-docs
rmdir $RPM_BUILD_ROOT%{_docdir}/libthai

rm $RPM_BUILD_ROOT%{_libdir}/*.la

%ldconfig_scriptlets

%check
make check

%files
%doc README AUTHORS COPYING ChangeLog
%{_libdir}/lib*.so.*
%{_datadir}/libthai


%files devel
%doc installed-docs/*
%{_includedir}/thai
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*

%files static
%{_libdir}/libthai.a

%changelog
* Tue Jul 30 2024 wangxiao <wangxiao184@h-partners.com> -0.1.29-2
- update release

* Sat Nov 5 2022 Qingqing Li <liqingqing3@huawei.com> -0.1.29-1
- upgrade to 0.1.29

* Wed Apr 27 2022 volcanodragon <linfeilong@huawei.com> -0.1.28-5
- enable check

* Thu Dec 17 2020 xinghe <xinghe1@huawei.com> -0.1.28-4
- correct source

* Fri Jan 10 2020 wuxu_wu <wuxu.wu@huawei.com> -0.1.28-3
- delte useless patch from package

* Mon Sep 10 2018 openEuler Buildteam <buildteam@openeuler.org> -0.1.28-2
- Package init





















